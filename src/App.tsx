import './App.css';
import ScreenUsers from './screens/ScreenUsers';

function App() {
  return (
    <div className="App">
        <ScreenUsers />
    </div>
  );
}

export default App;
