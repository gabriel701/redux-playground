import axios from "axios";

const baseURL = "http://localhost:5000";

export const getUser = async () => {
  return await axios
    .get(`${baseURL}/user`)
    .then((resp) => {
      
      return resp.data;
    })
    .catch((err) => {
      console.log("error getUser");
    });
};

export const getAccount = async () => {
    return await axios
      .get(`${baseURL}/account`)
      .then((resp) => {
      
        return resp.data;
      })
      .catch((err) => {
        console.log("error getAccount");
      });
  };
  export const getAccountAmount = async () => {
    return await axios
      .get(`${baseURL}/accountamount`)
      .then((resp) => {
      
        return resp.data;
      })
      .catch((err) => {
        console.log("error accountamount");
      });
  };
  