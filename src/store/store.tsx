import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit";
import uiReducer from "../slice/uiSlice";
import dataReducer from "../slice/dataSlice";

export const store = configureStore({
  reducer: {
    ui: uiReducer,
    data: dataReducer,
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
