import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import { UserModel } from "../models/UserModel";
import { AccountModel } from "../models/AccountModel";
import { AccountAmountModel } from "../models/AccountAmountModel";
import { getAccount, getAccountAmount, getUser } from "../api";
import { setLoading } from "./uiSlice";

interface IinitialState {
  user: UserModel[];
  account: AccountModel[];
  accountAmount: AccountAmountModel[];
  userSelect: String;
  accountFilter: AccountModel[];
  accountAmountFilter: AccountAmountModel[];
  accountAmountSelect: string;
  accountAmountchanges: AccountAmountModel[]
}

const initialState: IinitialState = {
  user: [],
  account: [],
  accountAmount: [],
  userSelect: "",
  accountAmountFilter: [],
  accountFilter: [],
  accountAmountSelect:"",
  accountAmountchanges: []
};

export const fetchUser = createAsyncThunk(
  "data/fetchUser",
  async (_, { dispatch }) => {
    dispatch(setLoading(true));
    setTimeout(async () => {
      const userData = await getUser();
      dispatch(setUser(userData));
      dispatch(setLoading(false));
    }, 500);
  }
);

export const fetchAccount = createAsyncThunk(
  "data/fetchAccount",
  async (_, { dispatch }) => {
    dispatch(setLoading(true));
    setTimeout(async () => {
      const userData = await getAccount();
      dispatch(setAccount(userData));
      dispatch(setLoading(false));
    }, 500);
  }
);

export const fetchAccountAmount = createAsyncThunk(
  "data/fetchAccountAmount",
  async (_, { dispatch }) => {
    dispatch(setLoading(true));
    setTimeout(async () => {
      const userData = await getAccountAmount();
      dispatch(setAccountAmount(userData));
      dispatch(setLoading(false));
    }, 500);
  }
);

export const uiSlice = createSlice({
  name: "ui",
  initialState,
  reducers: {
    setUser: (state, action) => {
      state.user = action.payload;
    },
    setAccount: (state, action) => {
      state.account = action.payload;
    },
    setAccountAmount: (state, action) => {
      state.accountAmount = action.payload;
    },
    setUserSelect: (state, action) => {
      state.userSelect = action.payload;
    },
    setAccountFilter: (state, action) => {
        state.accountFilter  = action.payload;
    },
    setAccountAmountFilter: (state, action) => {
      state.accountAmountFilter = action.payload;
    },
    setAccountAmountSelect : (state, action) => {
        state.accountAmountSelect = action.payload;
    },
    setAccountAmountUpdated :(state, action) =>{
      state.accountAmountchanges = [...state.accountAmountchanges , action.payload]
    }
  },
});

export const {
  setUser,
  setAccount,
  setAccountAmount,
  setUserSelect,
  setAccountFilter,
  setAccountAmountFilter,
  setAccountAmountSelect,
  setAccountAmountUpdated
} = uiSlice.actions;

export default uiSlice.reducer;
