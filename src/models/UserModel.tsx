export interface UserModel {
    userId: string;
    name: string;
    enable: boolean
}