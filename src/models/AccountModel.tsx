export interface AccountModel {
    id: string;
    name: string;
    state: string;
    userId: string;
}