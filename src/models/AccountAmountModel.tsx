export interface AccountAmountModel {
    id: string;
    amount: number;
    accountId: string;
}