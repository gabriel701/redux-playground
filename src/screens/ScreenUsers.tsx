import React, { useEffect } from "react";
import {
  fetchAccount,
  fetchAccountAmount,
  fetchUser,
} from "../slice/dataSlice";
import { useAppDispatch, useAppSelector } from "../hooks/useAppDispatch";
import { UserList } from "../components/users/UserList";
import { Spinner } from "../components/spinner/Spinner";
import { Col, Container, Row } from "react-bootstrap";
import { Divider, Typography } from "@mui/material";
import { AccountsComponent } from "../components/account/AccountsComponent";
import { ViewChangesList } from "../components/viewChanges/ViewChangesList";

export default function ScreenUsers() {
  const dispatch = useAppDispatch();
  const users = useAppSelector((state) => state.data.user);
  const loading = useAppSelector((state) => state.ui.loading);

  useEffect(() => {
    dispatch(fetchUser());
    dispatch(fetchAccount());
    dispatch(fetchAccountAmount());
    // eslint-disable-next-line
  }, []);

  return (
    <div>
      {loading && <Spinner />}

      <Container style={{ paddingTop: 15 }}>
        <Typography>Muestra</Typography>

        <Row>
          <Col md={4}>
            {!loading && <UserList key={1232131231313} userList={users} />}
          </Col>
          <Col>
            <AccountsComponent key={"accountKey"} />
          </Col>
        </Row>
        <Col className="md-12">
          <Row className="mt-2">
            <Divider className="mb-3" />
            <ViewChangesList  />
          </Row>
        </Col>
      </Container>
    </div>
  );
}
