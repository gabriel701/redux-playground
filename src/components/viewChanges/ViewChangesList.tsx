import React from "react";
import { useAppSelector } from "../../hooks/useAppDispatch";
import { Button } from "react-bootstrap";

export const ViewChangesList = () => {
  const amountChanges = useAppSelector(
    (state) => state.data.accountAmountchanges
  );

  return (
    <>
      {amountChanges.length > 0 && (
        <div className="container">
    
          <div className="col-12 ">
            <div className="row row-col-6">
              <Button>Guardar transacción</Button>
              <Button className="mt-3 btn btn-danger">Cancelar</Button>
            </div>
          </div>
        </div>
      )}
    </>
  );
};
