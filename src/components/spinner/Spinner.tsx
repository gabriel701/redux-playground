import React from 'react'

export const Spinner = () => {
  return (
    <div style={{flex:1, alignSelf: "stretch", fontSize:30}} >Cargando...</div>
  )
}
