import React, {  useState } from "react";
import { useAppDispatch, useAppSelector } from "../../hooks/useAppDispatch";
import {  setAccountAmountUpdated } from "../../slice/dataSlice";

export const AccountAmountComponent = () => {
  const dispatch = useAppDispatch();
  const accountAmountSelect = useAppSelector(
    (state) => state.data.accountAmountFilter
  );
  const accountAmmountFilter = useAppSelector(
    (state) => state.data.accountAmountFilter
  )[0];

  const [amount, setAmount] = useState(0)

  const handleClick = (event: any) => {
    event.preventDefault();
    console.log(event.target.montoId.value);
    dispatch(
      setAccountAmountUpdated({
        ...accountAmmountFilter,
        amount
      })
    );

  };

  const handlechange = (event: any) => {
    setAmount(event.target.value)
  };
  return (
    <>
      {accountAmountSelect.length > 0 && (
        <div className="card" style={{ marginTop: 100 }}>
          <div className="card-body">
            <form className="form-inline" onSubmit={handleClick}>
              <div className="form-group mb-2">
                <label style={{ display: "inline" }}>
                  Monto actual: {accountAmmountFilter.amount}
                  <input
                    style={{ marginTop: 10 }}
                    id="montoId"
                    className="form-control"
                    type="text"
                    onChange={handlechange}
                    placeholder="Nuevo Monto"
                  />
                </label>
                <button className="btn btn-primary mt-3" type="submit">
                  Actualizar
                </button>
              </div>
            </form>
          </div>
        </div>
      )}
    </>
  );
};
