import React from "react";
import { UserModel } from "../../models/UserModel";
import { User } from "./User";
import { Divider, Typography } from "@mui/material";
import { Card } from "react-bootstrap";

interface Props {
  userList: UserModel[];
}

export const UserList = ({ userList }: Props) => {
  return (
    <>
      <Card className="mx-2 ">
        <Typography
          style={{
            marginBottom: 10,
            marginTop: 10,
          }}
        >
          Lista de Usuario
        </Typography>
        <Divider />
        {userList.map((item) => {
          return <User user={item} />;
        })}
      </Card>
    </>
  );
};
