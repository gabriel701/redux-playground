import React from "react";
import { UserModel } from "../../models/UserModel";
import { Box, List, ListItem, ListItemButton, ListItemIcon, ListItemText, Divider } from "@mui/material";
import { useAppDispatch, useAppSelector } from "../../hooks/useAppDispatch";
import { setAccountFilter, setUserSelect } from "../../slice/dataSlice";

interface Props {
  user: UserModel;
}

export const User = ({ user }: Props) => {
    const dispatch = useAppDispatch();
    const accounts = useAppSelector((state) => state.data.account )


    const handleClick = (current: any) => {
        dispatch(setUserSelect(current))    
        const accountsFilter = accounts.filter(item => item.userId === current)
    
        dispatch(setAccountFilter(accountsFilter))

      };
  return (
    <>
      <Box sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}>
        
        <nav aria-label="secondary mailbox folders">
          <List>
            <ListItem disablePadding>
              <ListItemButton onClick={ () => handleClick(user.userId)} >
                <ListItemText primary={user.name} />
              </ListItemButton>
            </ListItem>
          </List>
        </nav>
        <Divider />
      </Box>
    </>
  );
};

