import { useAppDispatch, useAppSelector } from "../../hooks/useAppDispatch";
import { Typography } from "@mui/material";
import { DataGrid, GridColDef } from "@mui/x-data-grid";
import {
  setAccountAmountFilter,
  setAccountAmountSelect,
} from "../../slice/dataSlice";
import { AccountAmountComponent } from "../AccountAmount/AccountAmountComponent";

export const AccountsComponent = () => {
  const dispatch = useAppDispatch();
  const accFilter = useAppSelector((state) => state.data.accountFilter);
  const userSelected = useAppSelector((state) => state.data.userSelect);
  const accounAmount = useAppSelector((state) => state.data.accountAmount);

  const handlerSelect = (current: any) => {
    const idSelect = current[0] ? current[0].toString() : "";
    const accountAmmountFilter = accounAmount.filter(
      (item) => item.accountId === idSelect
    );
    dispatch(setAccountAmountFilter(accountAmmountFilter));
    dispatch(setAccountAmountSelect(idSelect));
  };

  const columns: GridColDef[] = [
    { field: "userId", headerName: "Usuario", width: 150 },
    { field: "name", headerName: "Nombre de la cuenta", width: 150 },
  ];

  return (
    <>
      {userSelected.length > 0 && (
        <div style={{ height: 350, width: "100%" }}>
          <Typography>Usuario {userSelected}</Typography>
          <DataGrid
            rows={accFilter}
            columns={columns}
            onRowSelectionModelChange={handlerSelect}
          />
        </div>
      )}
      <AccountAmountComponent key={1234} />
    </>
  );
};
